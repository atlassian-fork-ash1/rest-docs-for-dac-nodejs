var argv = require('minimist')(process.argv.slice(2));
var fs = require('fs-extra');
var path = require('path');

var processor = require('./lib/processor');

var basePath = argv.p || argv.path || '../..';
var target = argv.t || argv.target || path.resolve('./target');

if (argv._.length === 0 || argv._[0] === '') {
    throw new Error('You must provide a filename');
}

var filename = argv._[0];
console.log('Reading from ' + filename);
var html = fs.readFileSync(filename);
var output = processor(html, basePath);
if (argv.d || argv.debug) {
    console.log(output);
} else {
    fs.mkdirpSync(target);
    var outputFilename = path.join(target, path.basename(filename));
    console.log('Writing to ' + outputFilename);
    fs.writeFileSync(outputFilename, output);
}



